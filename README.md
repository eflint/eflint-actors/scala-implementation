# scala-implementation

Implementation of "normative actors" using Scala Akka framework.

### Installation

Can be installed for use with `sbt` using `sbt publishLocal`.

Requires:

* Haskell implementation of eFLINT: [click here](https://gitlab.com/eflint/haskell-implementation)
* Java implementation of eFLINT-server: [click here](https://gitlab.com/eflint/eflint-actors/java-implementation)