ThisBuild / scalaVersion     := "2.13.3"
ThisBuild / organization     := "eflint"
name := "scala-server"
ThisBuild / version          := "0.1.10"

Compile / scalaSource := baseDirectory.value / "src"

fork in run := true

lazy val AkkaVersion = "2.6.21"

lazy val root = (project in file("."))
  .settings(
    libraryDependencies += "eflint" %% "java-server" % "0.1.11-SNAPSHOT" ,
    libraryDependencies += "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion ,
    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3",
    libraryDependencies += "com.googlecode.json-simple" % "json-simple" % "1.1.1"
  )

scalacOptions ++= Seq("-feature", "-deprecation")

// Uncomment the following for publishing to Sonatype.
// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for more detail.

// ThisBuild / description := "Some descripiton about your project."
// ThisBuild / licenses    := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))
// ThisBuild / homepage    := Some(url("https://github.com/example/project"))
// ThisBuild / scmInfo := Some(
//   ScmInfo(
//     url("https://github.com/your-account/your-project"),
//     "scm:git@github.com:your-account/your-project.git"
//   )
// )
// ThisBuild / developers := List(
//   Developer(
//     id    = "Your identifier",
//     name  = "Your Name",
//     email = "your@email",
//     url   = url("http://your.url")
//   )
// )
// ThisBuild / pomIncludeRepository := { _ => false }
// ThisBuild / publishTo := {
//   val nexus = "https://oss.sonatype.org/"
//   if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
//   else Some("releases" at nexus + "service/local/staging/deploy/maven2")
// }
// ThisBuild / publishMavenStyle := true
