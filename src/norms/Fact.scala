package norms

import util.Value
import akka.actor.typed.ActorRef

trait Fact {}
trait Event  extends Fact {}
trait Action extends Event {}
trait Duty extends Fact {}

case class StringAction[T,U](actor:MaybeActorRef[T],recipient:MaybeActorRef[U],string:String) extends Action {}
case class StringDuty[T,U](holder:MaybeActorRef[T],claimant:MaybeActorRef[U],string:String) extends Duty {}
case class StringEvent[T,U](holder:MaybeActorRef[T],claimant:MaybeActorRef[U],string:String) extends Event {}

case class ActionValue[T,U](actor:MaybeActorRef[T],recipient:MaybeActorRef[U],value:Value) extends Action {}
case class DutyValue[T,U](holder:MaybeActorRef[T],claimant:MaybeActorRef[U],value:Value) extends Duty {}
case class EventValue[T,U](holder:MaybeActorRef[T],claimant:MaybeActorRef[U],value:Value) extends Event {}
