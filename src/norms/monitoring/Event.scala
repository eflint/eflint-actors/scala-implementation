package norms.monitoring;

import norms._
import akka.actor.typed.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorRefResolver
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl._
import scala.concurrent.duration._

trait Event {}

trait Request[R] extends Event {
  val timeout:FiniteDuration
  val default:R
  val by:ActorRef[R]

  case class NoResponse() extends NormActor.QueryResponse
  var listeners:Int = 0;
  def make_listener(cons:(NormActor.QueryResponse=>R)):Behavior[NormActor.QueryResponse] = 
   Behaviors.setup { context => 
      Behaviors.withTimers { timers =>
        if (!timers.isTimerActive(listeners)) {
          timers.startSingleTimer(listeners, NoResponse(), timeout)
          listeners += 1
        }
        Behaviors.receive { (context, message) => { 
          message match {
            case m:NoResponse => {
              by ! default 
              Behaviors.ignore
            }
            case m:NormActor.Response => {
              by ! cons(m)
              Behaviors.ignore
            }
          }
        }
      }
    }
  }
}


