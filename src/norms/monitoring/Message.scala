package norms.monitoring;

import akka.actor.typed.ActorRef

case class Message[M](from:ActorRef[_], to:ActorRef[M], msg:M) extends Event {
  def apply()(implicit monitors:List[ActorRef[Event]]) {
    to ! msg
    for (monitor <- monitors) {
      monitor ! this
    }
  }
}
