package norms.events;

trait Universe {
  type T<:Time[T]
  val bigBang:()=>T
}

trait Time[T] {
  val prev:Option[T]

  def holds(fluent:Fluent[Any]):Boolean
  def postulate(fluent:Fluent[Any]):T
  def discount(fluent:Fluent[Any]):T
}

trait FluentDecl[T<:Time[T]] {
  def holdsAt(time:T):Boolean
}

trait DutyDecl[H,C,T<:Time[T]] extends FluentDecl[T] {
  def holderOf():H
  def claimantOf():C
  def violatedAt(time:T):Boolean
}

trait EventDecl[T<:Time[T]] extends FluentDecl[T] {
  def enabledAt(time:T)
  def initiatesAt(time:T):Set[Fluent[Any]]
  def terminatesAt(time:T):Set[Fluent[Any]]
}

trait ActionDecl[P,R,T<:Time[T]] extends EventDecl[T] {
  def actorOf():P
  def performerOf():P = {this.actorOf()}
  def recipientOf():Set[R] // statically computed from performers/holders of initiated/terminated actions/duties
}

trait Fluent[+F] {
  val fluent:F
}

trait Duty[+D] {
  val duty:D
}

trait Event[+E] {
  val event:E
}

trait Action[+A] {
  val action:A
}

abstract class CreateEventDecl[T<:Time[T]] extends EventDecl[T] with Event[CreateEvent[T]] {
  def holdsAt(trace:T) = true
  def enabledAt(trace:T) = !trace.holds(event.fluent)
  def initiatesAt(trace:T) = Set(event.fluent)
  def terminatesAt(trace:T) = Set()
}
case class CreateEvent[T<:Time[T]](fluent:Fluent[Any]) extends CreateEventDecl[T] {
  override val event = this
}

abstract class TerminateEventDecl[T<:Time[T]] extends EventDecl[T] with Event[TerminateEvent[T]] {
  def holdsAt(trace:T) = true
  def enabledAt(trace:T) = trace.holds(event.fluent)
  def initiatesAt(trace:T) = Set()
  def terminatesAt(trace:T) = Set(event.fluent)
}

case class TerminateEvent[T<:Time[T]](fluent:Fluent[Any]) extends TerminateEventDecl[T] {
  override val event = this
}

object ConfigSpecs extends Universe{
  case class Trace(cfg:Set[Fluent[Any]], prev:Option[Trace]) extends Time[Trace] {
    def discount(fluent:Fluent[Any]) = Trace(cfg - fluent, Some(this))
    def postulate(fluent:Fluent[Any]) = Trace(cfg + fluent, Some(this))
    def holds(fluent:Fluent[Any]) = cfg.contains(fluent)
  }
  type T = Trace
  val bigBang = () => Trace(Set(), None)
}

import ConfigSpecs._

abstract class PersonDecl extends FluentDecl[Trace] with Fluent[Person] {
  def holdsAt(t:Trace) = t.cfg.contains(fluent)
}

case class Person(value:String) extends PersonDecl {
  override val fluent = this
}

object Test extends App {
  val p:Person = Person("Jan")
  println(p.holdsAt(Trace(Set(Person("Jan")), None)))
}

/*
abstract class DutyCons[D,H,C,T](value:D) extends DutyDecl[D,H,C,T] {}
abstract class EventCons[E,T](value:E) extends EventDecl[E,T] {}
abstract class ActionCons[A,P,R,T](value:A) extends ActionDecl[A,P,R,T] {}

class PersonDecl extends FluentDecl[Person,Cfg] {
  def holdsAt(cfg:Cfg) = True

}

case class NaturalParentOf(parent:Person, child:Person) 
class NaturalParentOfDecl extends FluentDecl[NaturalParentOf,Cfg]

*/
