package norms

trait Message {}
final case class ViolatedAction(action : norms.Action) extends Message
final case class ExecutedAction(action : norms.Action) extends Message
final case class ViolatedDuty(duty : norms.Duty) extends Message
final case class ActiveDuty(duty : norms.Duty) extends Message 

