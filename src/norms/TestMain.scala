package norms; 

import akka.actor.typed.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorRefResolver
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl._
import akka.util.Timeout
import scala.concurrent.duration._

object TestApp extends App {
  val testMain: ActorSystem[TestMain.Scenarios] = ActorSystem(TestMain(), "NormActor-experiments")
  val resolver = ActorRefResolver(testMain)
  testMain ! TestMain.test1()
}

object TestMain {
  sealed trait Scenarios
  final case class test1() extends Scenarios

  def apply() : Behavior[Scenarios] =
   Behaviors.setup { context => Behaviors.receive { (context, message) => 
    implicit val resolver:ActorRefResolver = TestApp.resolver

    message match {
      case m:test1 => {
      val gdpr_actor = new NormActor("apps/banking/bank_gdpr.eflint", List(
                                     "apps/banking/bank_gdpr_specialization.eflint"
//                                    ,"apps/banking/bank_gdpr_domain1.eflint"
                                    ))
      val gdpr_actor_ref = context.spawn(gdpr_actor.listen(), "GDPR1")
      Thread.sleep(200)
      val george_ref = context.spawn(new Sink("George")(), "George")
      val banka_ref = context.spawn(new Sink("BankA")(), "BankA")
      val bankb_ref = context.spawn(new Sink("BankB")(), "BankB")
      george_ref ! Sink.Drain() // just to test
      val george = "\"" + resolver.toSerializationFormat(george_ref) + "\""
      val banka  = "\"" + resolver.toSerializationFormat(banka_ref) + "\""
      val bankb  = "\"" + resolver.toSerializationFormat(bankb_ref) + "\""
      gdpr_actor_ref ! NormActor.Phrase(s"+subject($george)")
      gdpr_actor_ref ! NormActor.Phrase(s"+data($george, Country, Chili)")
      gdpr_actor_ref ! NormActor.Phrase(s"+data($george, SBI, Chemical)")
      gdpr_actor_ref ! NormActor.Phrase(s"+accurate-for-purpose()")
      gdpr_actor_ref ! NormActor.Phrase(s"+controller($banka)")
      gdpr_actor_ref ! NormActor.Phrase(s"+controller($bankb)")
      gdpr_actor_ref ! NormActor.Phrase(s"+processor(controller)")
      gdpr_actor_ref ! NormActor.Phrase(s"give-consent(subject=$george,controller=$bankb)")
      gdpr_actor_ref ! NormActor.Phrase(s"collect-personal-data(controller=$bankb,subject=$george,processor=$bankb, data=data($george,Country,Chili))")
      gdpr_actor_ref ! NormActor.Phrase(s"collect-personal-data(controller=$bankb,subject=$george,processor=$bankb, data=data($george,SBI,Chemical))")
      gdpr_actor_ref ! NormActor.Phrase(s"+value(Gambling). data-change($george,SBI,Gambling)")
      gdpr_actor_ref ! NormActor.Phrase(s"demand-rectification(subject=$george,controller=$bankb)")
      gdpr_actor_ref ! NormActor.Phrase(s"rectification-delay(subject=$george,controller=$bankb)")
      gdpr_actor_ref ! NormActor.Phrase(s"collect-personal-data(controller=$bankb,subject=$george,processor=$bankb, data=data($george,SBI,Gambling))")
      gdpr_actor_ref ! NormActor.Phrase(s"rectify-personal-data(processor=$bankb,controller=$bankb,subject=$george)")
      gdpr_actor_ref ! NormActor.Phrase(s"?Enabled(collect-personal-data(controller=$banka,subject=$george,processor=$banka, data=data($george,SBI,Chemical)))")
      gdpr_actor_ref ! NormActor.Phrase(s"collect-personal-data(controller=$banka,subject=$george,processor=$banka, data=data($george,SBI,Chemical))")
      gdpr_actor_ref ! NormActor.Phrase(s"Force collect-personal-data(controller=$banka,subject=$george,processor=$banka, data=data($george,SBI,Chemical))")
      implicit val responseTimeout:Timeout = 2.seconds
      gdpr_actor_ref ! NormActor.Kill()
      Thread.sleep(3000)
      Behaviors.stopped
     }
    }
  }}
}

object Sink {
  trait Message extends norms.Message
  case class Drain() extends Message
//  case class Normative(msg:norms.Message) extends Message
}

class Sink(name:String) {
  import Sink._

  def apply(): Behavior[norms.Message] = Behaviors.receive { (context, message) => {
    message match {
      case ViolatedAction(StringAction(s,r,msg))  => {
        if (context.self.equals(s)) context.log.info("violated action: " + msg)
      }
      case ViolatedDuty(StringDuty(h,c,msg))      => {
        if (context.self.equals(h)) context.log.info("violated duty: " + msg)
      }
      case ActiveDuty(StringDuty(h,c,msg))        => {
        if (context.self.equals(h)) context.log.info("new active duty: " + msg)
      }
      case Drain() => {

      }
    }
    Behaviors.same
  }}
}
