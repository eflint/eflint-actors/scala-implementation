package norms;

import util._ // from eflint.java-server
import org.json.simple._; // from json-simple
import java.util.ArrayList;
import collection.JavaConverters._

import akka.actor.typed.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorRefResolver
import akka.actor.typed.Behavior
import akka.actor.typed.PostStop
import akka.actor.typed.Signal
import akka.actor.typed.scaladsl.Behaviors

object NormActor{
  sealed trait Message
  case class Phrase(phrase:String, handler:ActorRef[norms.Message]=null) extends Message
  case class Query(replyTo:ActorRef[QueryResponse], phrase:String) extends Message
  case class Kill() extends Message
  trait QueryResponse
  case class Response(success:Boolean, reason:String) extends QueryResponse

  def to_address[T](actor:ActorRef[T])(implicit resolver:ActorRefResolver):String = {
    return "\"" + resolver.toSerializationFormat(actor) + "\""
  }
}

class NormActor(file:String=null
               ,configs:List[String]=List()
               ,handler:ActorRef[norms.Message]=null
               ,debug_mode:Boolean = false)
               (implicit resolver:ActorRefResolver) {
  import NormActor._

  val eflint = new EFLINT(file, debug_mode)

  for (fp <- configs) {
    val source = scala.io.Source.fromFile(fp)
    val lines = try source.mkString finally source.close()  
    if (lines != null) {
      try {
        val ss:ServerState = this.eflint.phrase(lines)
        produce_output_events(ss)
        report_errors(ss.get_errors().asScala.toList, lines, debug_mode)
      } catch {
        case e:InvalidEFlintInput => {
          println(e.toString())
          println(lines)
        }
      }
    }
  }

  def listen() : Behavior[Message] = Behaviors.setup { context =>
    step()
  }

  def step() : Behavior[Message] = {
   Behaviors
    .receive[Message] { (context, message) => 
      message match {
        case m:Phrase => {
          try {
            val ss:ServerState = this.eflint.phrase(m.phrase)
            produce_output_events(ss, m.handler)
            report_errors(ss.get_errors().asScala.toList, m.phrase, debug_mode)
          }
          catch {
            case e:InvalidEFlintInput => {
              context.log.info(e.toString())
              context.log.info(m.phrase)
            }
          }
          step()
        }
        case m:Query => {
          try {
            val res = this.eflint.phrase(m.phrase)
            if (res.query_res == null)
              m.replyTo ! Response(false, "no query submitted")
            else if (res.query_res == true) 
              m.replyTo ! Response(true, "eflint said so")
            else if (res.query_res == false)
              m.replyTo ! Response(false, "eflint said so")
          }
          catch {
            case e:Throwable => m.replyTo ! Response(false, e.toString())
          }
          step()
        }
        case m:Kill => {
          this.eflint.close()
          Behaviors.stopped 

        } 
      }
    }
    .receiveSignal { 
      case (context, PostStop) => {
        this.eflint.close()
        Behaviors.same
      }
    }
 }

  def report_errors(errors:List[EFLINTError], phrase:String, debug_mode:Boolean) = {
    for (err:EFLINTError <- errors) {
      err match {
        case error:NonDeterministicTransition =>
          if (debug_mode)
            print("ambiguous trigger " ++ phrase)
        case error:CompilationError => {
          if (debug_mode)
            print("eflint compilation error: " ++ error.message)
        }
        case _ => {}
      }
    }
  }

  def report_trigger_violation(value:Value, forced:Boolean, handler:ActorRef[norms.Message])
    (implicit resolver:ActorRefResolver)= {
    val verb = if (forced) "performed" else "tried"
    if (value.arguments.size() > 1) {
      val actor     = find_ActorRef(value.arguments.get(0))
      val recipient = find_ActorRef(value.arguments.get(1))
      actor match {
        case Right(ref) => ref ! ViolatedAction(ActionValue(actor,recipient,value)) 
        case _ => 
      } 
      recipient match {
        case Right(ref) => ref ! ViolatedAction(ActionValue(actor,recipient,value))
        case _ => 
      }
      if (handler != null) {
        handler ! ViolatedAction(ActionValue(actor, recipient, value))
      }
    }
  }

  def report_executed_action(value:Value, handler:ActorRef[norms.Message])
                            (implicit resolver:ActorRefResolver)= {
    if (value.arguments.size() > 1) {
      val actor:MaybeActorRef[norms.Message]     = find_ActorRef(value.arguments.get(0))
      val recipient:MaybeActorRef[norms.Message] = find_ActorRef(value.arguments.get(1))
      actor match {
        case Right(ref) => ref ! ExecutedAction(ActionValue(actor,recipient,value)) 
        case _ => 
      } 
      recipient match {
        case Right(ref) => ref ! ExecutedAction(ActionValue(actor,recipient,value))
        case _ => 
      }
      if (handler != null) {
        handler ! ExecutedAction(ActionValue(actor, recipient, value))
      }
    }
  }

  def report_duty_violation(duty:Value, handler:ActorRef[norms.Message]) {
    if (duty.arguments.size() > 1) {
      val holder    = find_ActorRef(duty.arguments.get(0))
      val claimant  = find_ActorRef(duty.arguments.get(1))
      holder match {
        case Right(ref) => ref ! ViolatedDuty(DutyValue(holder,claimant,duty))
        case _ => 
      }
      claimant match {
        case Right(ref) => ref ! ViolatedDuty(DutyValue(holder,claimant,duty))
        case _ => 
      }
      if (handler != null) {
        handler! ViolatedDuty(DutyValue(holder,claimant,duty))
      }
    }
  }

  def report_new_duty(duty:Value, handler:ActorRef[norms.Message]) {
    if (duty.arguments.size() > 1) {
      val holder    = find_ActorRef(duty.arguments.get(0))
      val claimant  = find_ActorRef(duty.arguments.get(1))
      holder match {
        case Right(ref) => ref ! ActiveDuty(DutyValue(holder,claimant,duty))
        case _ => 
      }
      claimant match {
        case Right(ref) => ref  ! ActiveDuty(DutyValue(holder,claimant,duty))
        case _ => 
      }
      if (handler != null) {
        handler ! ActiveDuty(DutyValue(holder,claimant,duty))
      }
    }
  }

  var prev_violations:Set[Violation] = (new ArrayList[Violation]()).asScala.toSet
  def produce_output_events(state:ServerState, handler:ActorRef[norms.Message]=null) = {
    for (violation:Violation <- state.get_violations().asScala) {
      violation match {
        case v:TriggerViolation => report_trigger_violation(v.info.value, true, handler)
        case v:DutyViolation   => {
          if (!prev_violations.exists(other => other match { // report only new duty violations 
              case o:DutyViolation => o.duty.equals(v.duty)
              case _ => false
            })) {
            report_duty_violation(v.duty, handler)
          }
        }
        case e:InvariantViolation => {
          e.print() 
        }
      }
    }
    prev_violations = state.get_violations().asScala.toSet
    for (v:Value <- state.get_new_duties().asScala) {
      report_new_duty(v, handler)
    }

    state.get_output_events().asScala.foreach { ea =>
        if (ea.info.is_action) report_executed_action(ea.info.value, handler)
    }
  }

  private def find_ActorRef(value:Value)(implicit resolver:ActorRefResolver):MaybeActorRef[norms.Message] = {
    val str:String = 
          if (value.arguments == null) value.string_value
          else // address is expected at location 0 
               value.arguments.get(0).string_value
    val ref:ActorRef[norms.Message] = resolver.resolveActorRef[norms.Message](str) 
    if (ref.path.name.equals("deadLetters"))
      return Left(str);
    return Right(ref);
  }
}
