
import akka.actor.typed.ActorRef

package object norms {
  type MaybeActorRef[T] = Either[String,ActorRef[T]]
}
