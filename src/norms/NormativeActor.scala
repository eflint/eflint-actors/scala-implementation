package norms.events;

import akka.actor.typed.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorRefResolver
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors

object NormativeActor {
  trait Message[T<:Time[T]] { val sender:ActorRef[_<:ResponseMessage[T]] }
  case class FactObservation[T<:Time[T]] 
             (sender:ActorRef[ObservationResponse[T]]
             ,fluent:Fluent[Any]
             ,observation:Boolean) extends Message[T]
  case class EventObservation[T<:Time[T]]
             (sender:ActorRef[ObservationResponse[T]]
             ,event:Event[Any] with EventDecl[T]) extends Message[T]
  
  trait ResponseMessage[T<:Time[T]]
  case class ObservationResponse[T<:Time[T]](trace:T) extends ResponseMessage[T]

  trait NormUpdate[T<:Time[T]] extends ResponseMessage[T]
}

class NormativeActor[T<:Time[T]] {
  import NormativeActor._

  def apply(trace:T) : Behavior[Message[T]] = Behaviors.setup { context => 
    listen(trace)
  }

  def listen(trace:T):Behavior[Message[T]] = Behaviors.receive { (context, message) =>
    message match {
      case m:FactObservation[T] => {
        val new_trace:T = if(m.observation) trace.postulate(m.fluent) else trace.discount(m.fluent)
        m.sender ! ObservationResponse[T](new_trace)
        listen(new_trace)
      } 
      case m:EventObservation[T] => {
        val new_trace = trace
        listen(new_trace)
      }
    }
  }

  private def transition(trace:T, terms:Fluent[Any], creates:Fluent[Any]) {
    
  }
}
