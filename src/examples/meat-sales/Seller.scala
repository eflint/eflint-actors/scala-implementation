package examples.meat_sales;

import norms._

import akka.actor.typed.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorRefResolver
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl._

import scala.util.Random

object Seller {
  trait Message extends norms.Message {}
}

class Seller {
  import Seller._

  def listen(contract_ref : ActorRef[Contract.Message]) : Behavior[norms.Message] = 
   Behaviors.receive { (context, message) => 
    message match {
      case ActiveDuty(DutyValue(Right(h),Right(c),msg)) => {
        if (context.self.equals(h) && msg.fact_type.equals("duty-to-deliver") && Random.nextBoolean()) {
          contract_ref ! Contract.DeliverMeat(context.self, "Home")
        } 
        listen(contract_ref)
      }
      case ViolatedDuty(DutyValue(Right(h),Right(c),msg)) => {
        if (context.self.equals(h)) println("violated duty: " + msg.toString())
        if (context.self.equals(c)) {
          contract_ref ! Contract.SuspendDelivery(context.self)
          contract_ref ! Contract.DemandInterest(context.self)
        }
        listen(contract_ref)
      }
      case ViolatedAction(ActionValue(Right(a),Right(r),msg)) => {
        if (context.self.equals(a)) println("violated action: " + msg.toString())
        listen(contract_ref)
      }
      case ExecutedAction(ActionValue(Right(a),Right(r),msg)) => {
        listen(contract_ref)
      }
    }
  }
}
