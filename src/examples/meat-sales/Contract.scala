package examples.meat_sales;

import norms.NormActor;

import akka.actor.typed.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorRefResolver
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl._
import akka.util.Timeout
import scala.concurrent.duration._

object Contract {
  trait Message {}
  case class DeliveryDue() extends Message
  case class PaymentDue() extends Message

  case class DemandInterest(sender:ActorRef[Seller.Message]) extends Message
  case class DeliverMeat(sender:ActorRef[Seller.Message], address:String) extends Message
  case class PayForMeat(sender:ActorRef[Buyer.Message], amount:Int) extends Message
  case class SuspendDelivery(sender:ActorRef[Seller.Message]) extends Message

  case class PaymentTimer(id:Contract)
  case class DeliveryTimer(id:Contract)
}

class Contract(buyer:Buyer
              ,seller:Seller
              ,amt:Int
              ,payDueDate:Int
              ,delAdd:String
              ,delDueDate:Int
              ,intRate:Int) {
  import Contract._
  val late_amt = (1 + intRate/100)*amt
  val payTimer = PaymentTimer(this)
  val delTimer = DeliveryTimer(this)

  def start()(implicit resolver:ActorRefResolver) : Behavior[Message] = Behaviors.setup { context => 
    val buyer_ref   = context.spawn(buyer.listen(context.self), "Buyer")
    val seller_ref  = context.spawn(seller.listen(context.self), "Seller")

    val eflint_server = new NormActor("apps/symboleo-comparison/meat-sales.eflint")
    val server_ref = context.spawn(eflint_server.listen(), "eFLINT-actor")

    val buyer_id   = "\"" + resolver.toSerializationFormat(buyer_ref) + "\""
    val seller_id  = "\"" + resolver.toSerializationFormat(seller_ref) + "\""
  
    server_ref ! NormActor.Phrase(s"Fact buyer Identified by $buyer_id") 
    server_ref ! NormActor.Phrase(s"Fact seller Identified by $seller_id")
    server_ref ! NormActor.Phrase(s"Fact address Identified by $delAdd")
    server_ref ! NormActor.Phrase(s"+delivery-address($buyer_id,$delAdd)")
    server_ref ! NormActor.Phrase(s"+normal-sum(sum($amt,CAD))")
    server_ref ! NormActor.Phrase(s"+late-sum(sum($late_amt,CAD))")
    server_ref ! NormActor.Phrase(s"+goods(meat(Beef,10,AAA))")
    server_ref ! NormActor.Phrase(s"+pay-for-meat(sum = late-sum.sum)")
    server_ref ! NormActor.Phrase(s"+pay-for-meat(sum = normal-sum.sum)")
    server_ref ! NormActor.Phrase(s"+deliver-meat()")
    server_ref ! NormActor.Phrase(s"+duty-to-deliver()")
    server_ref ! NormActor.Phrase(s"+duty-to-pay(sum = normal-sum.sum)")

    Behaviors.withTimers { timers =>
      if (!timers.isTimerActive(delTimer)) {
        timers.startSingleTimer(delTimer, DeliveryDue(), new FiniteDuration(delDueDate,SECONDS))
      }
      if (!timers.isTimerActive(payTimer)) {
        timers.startSingleTimer(payTimer, PaymentDue(), new FiniteDuration(payDueDate,SECONDS))
      }
      listen(server_ref, buyer_ref, seller_ref, buyer_id, seller_id) 
    }
  }

  def listen(server_ref:ActorRef[NormActor.Message]
            ,buyer_ref:ActorRef[Buyer.Message], seller_ref:ActorRef[Seller.Message]
            ,buyer_id:String, seller_id:String) : Behavior[Message] = Behaviors.receive { (context, message) =>
   message match {
    case m:DeliveryDue => {
      server_ref ! NormActor.Phrase(s"+delivery-due(goods.meat, $delAdd)")
      listen(server_ref, buyer_ref, seller_ref, buyer_id, seller_id)
    }
    case m:PaymentDue => {
      server_ref ! NormActor.Phrase(s"+payment-due(normal-sum.sum, $buyer_id, $seller_id)")
      listen(server_ref, buyer_ref, seller_ref, buyer_id, seller_id)
    }
    case m:DeliverMeat => {
      if (m.sender == seller_ref) {
        server_ref ! NormActor.Phrase(s"deliver-meat($seller_id, $buyer_id, ${m.address})")
      }
      listen(server_ref, buyer_ref, seller_ref, buyer_id, seller_id)
    }
    case m:DemandInterest => {
      if (m.sender == seller_ref) {
        server_ref ! NormActor.Phrase(s"demand-interest($seller_id, $buyer_id)")
      }
      listen(server_ref, buyer_ref, seller_ref, buyer_id, seller_id)
    }
    case m:SuspendDelivery => {
      if (m.sender == seller_ref) {
        server_ref ! NormActor.Phrase(s"suspend-delivery($seller_id, $buyer_id)")
      }
      listen(server_ref, buyer_ref, seller_ref, buyer_id, seller_id)
    }
    case m:PayForMeat => {
      if (m.sender == buyer_ref) {
        server_ref ! NormActor.Phrase(s"pay-for-meat($buyer_id, $seller_id, sum(${m.amount},CAD))")
      }
      listen(server_ref, buyer_ref, seller_ref, buyer_id, seller_id)
    }
   }
  }
}
