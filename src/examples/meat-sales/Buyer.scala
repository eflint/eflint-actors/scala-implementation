package examples.meat_sales;

import norms._

import akka.actor.typed.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorRefResolver
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl._

import scala.util.Random

object Buyer {
  trait Message extends norms.Message {}
}

class Buyer {
  import Buyer._ 

  def listen(contract_ref : ActorRef[Contract.Message]) : Behavior[norms.Message] = 
   Behaviors.receive { (context, message) => 
    message match {
      case ActiveDuty(DutyValue(Right(h),Right(c),msg)) => {
        if (context.self.equals(h) && msg.fact_type.equals("duty-to-pay") && Random.nextBoolean()) {
          contract_ref ! Contract.PayForMeat(context.self, 50) 
        }
        listen(contract_ref)
      }
      case ViolatedDuty(DutyValue(Right(h),Right(c),msg)) => {
        if (context.self.equals(h)) println("violated duty: " + msg.toString())
        listen(contract_ref)
      }
      case ViolatedAction(ActionValue(Right(a),Right(r),msg)) => {
        if (context.self.equals(a)) println("violated action: " + msg.toString())
        listen(contract_ref)
      }
      case ExecutedAction(_) => {
        listen(contract_ref)
      }
    }
  }
}
