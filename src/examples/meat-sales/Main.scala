package examples.meat_sales; 

import akka.actor.typed.ActorSystem
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorRefResolver
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl._
import akka.util.Timeout
import scala.concurrent.duration._

object TestApp extends App {
  val testMain: ActorSystem[TestMain.Scenarios] = ActorSystem(TestMain(), "MeatSales-experiments")
  val resolver = ActorRefResolver(testMain)
  testMain ! TestMain.test1()
}

object TestMain {
  sealed trait Scenarios
  final case class test1() extends Scenarios

  def apply() : Behavior[Scenarios] =
   Behaviors.setup { context => Behaviors.receive { (context, message) => 
    implicit val resolver:ActorRefResolver = TestApp.resolver
    message match {
     case m:test1 => {
      val buyer = new Buyer()
      val seller = new Seller()
      val contract = new Contract(buyer,seller,50,2,"Home",4,15)
      val contract_ref = context.spawn(contract.start(), "Contract")
     
      Thread.sleep(5000)
      Behaviors.stopped
     }
    }
  }}
}


